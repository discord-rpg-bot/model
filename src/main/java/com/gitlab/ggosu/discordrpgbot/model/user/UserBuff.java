package com.gitlab.ggosu.discordrpgbot.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
public class UserBuff {
    private Integer id;
    private Date timestamp;
}
