package com.gitlab.ggosu.discordrpgbot.model.user.global;

import com.gitlab.ggosu.discordrpgbot.model.user.UserBuff;
import com.gitlab.ggosu.discordrpgbot.model.user.UserItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("globalUser")
@AllArgsConstructor
@Getter
@Setter
public class GlobalUser {
    @Id
    private String userId;
    private List<UserItem> premiumItems;
    private List<UserBuff> premiumBuffs;
}
