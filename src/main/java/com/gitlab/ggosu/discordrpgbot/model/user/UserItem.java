package com.gitlab.ggosu.discordrpgbot.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class UserItem {
    private Integer id;
    private Integer amount;
}
