package com.gitlab.ggosu.discordrpgbot.model.unit.stats;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class UnitStats {
    private Integer strength;
    private Integer agility;
    private Integer intelligence;
    private Integer vitality;
    private Integer armor;
    private Integer magicResistance;
    private Float criticalChance;
    private Float criticalDamage;
}
