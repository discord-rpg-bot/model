package com.gitlab.ggosu.discordrpgbot.model.user.guild;

import com.gitlab.ggosu.discordrpgbot.model.user.UserBuff;
import com.gitlab.ggosu.discordrpgbot.model.user.UserItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("guildUser")
@AllArgsConstructor
@Getter
@Setter
public class GuildUser {
    @Id
    private String guildId_userId;

    private Byte characterActiveId;
    private List<Byte> characterIds;

    private List<UserItem> items;
    private List<UserBuff> buffs;
}
