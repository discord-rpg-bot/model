package com.gitlab.ggosu.discordrpgbot.model.unit;

import com.gitlab.ggosu.discordrpgbot.model.unit.stats.UnitStats;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("unitClass")
@AllArgsConstructor
@Getter
@Setter
public class UnitClass {
    @Id
    private Integer id;
    private String name;
    private String description;
    private String iconSmallURL;
    private String iconBigURL;

    private UnitStats baseStats;
    private UnitStats scaleStatsPerLevel;
}
